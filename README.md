Links:  https://cloud.google.com/security-command-center/docs/how-to-enable-real-time-notifications#sendgrid-email



Steps:

*   create a SendGrid account
        * you'll need to set up 2-factor auth with them

*   at https://console.cloud.google.com/ search for "SendGrid Email API"
        * select Free plan > Subscribe
        * Register with SendGrid
        * refresh the GCP page and click the "Manage on Provider" button > OK

*   SendGrid > Email API > Integration Guide > Web API > Python
        > (enter a key name) "Garden Email" > Create Key button

        * follow the steps to create the key as a file

Verify Single Sender
    https://docs.sendgrid.com/ui/sending-email/sender-verification

    https://app.sendgrid.com/settings/sender_auth

        Click > Single Sender Verification






pip install sendgrid

cmd>>    python --version
cmd>>    pip install certifi
cmd>>    /Applications/Python\ 3.10/Install\ Certificates.command            (im using Pyhton 3.10.0)








