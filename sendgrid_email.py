import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

credentials_path = '/Users/ryan/Code/YouTube/pubsub-to-sendgrid-email/sendgrid.private.key'
with open(credentials_path) as file:
    key = file.read()
    os.environ["SENDGRID_API_KEY"] = key

message = Mail(
    from_email='garden@ryanlogsdon.com',                    # sender address needs to be verified at https://app.sendgrid.com/settings/sender_auth
    to_emails='garden@ryanlogsdon.com',
    subject='Garden test',
    html_content='<strong>email test!</strong>')

try:
    # sg = SendGridAPIClient('YOU_CAN_ALSO_JUST_PASTE_YOUR_KEY_HERE')
    sg = SendGridAPIClient(os.environ['SENDGRID_API_KEY'])
    response = sg.send(message)
    print(response.status_code)
    print(response.body)
    print(response.headers)
except Exception as e:
    print(e)
